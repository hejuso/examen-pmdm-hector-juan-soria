package com.example.hejuso.splashproyecto.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hejuso.splashproyecto.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JuegoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JuegoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JuegoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_NICKNAME = "param1";
    private static final String ARG_NOMBRE = "param2";
    private static final String ARG_PUNTOS = "param3";

    // TODO: Rename and change types of parameters
    private String mparam1;
    private String mparam2;
    private String mparam3;

    private OnFragmentInteractionListener mListener;

    public JuegoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param nombre Parameter 1.
     * @param nickname Parameter 2.
     * @param puntos Parameter 2.
     * @return A new instance of fragment JuegoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JuegoFragment newInstance(String nombre, String nickname, String puntos) {
        JuegoFragment fragment = new JuegoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NICKNAME, nombre);
        args.putString(ARG_NOMBRE, nickname);
        args.putString(ARG_PUNTOS, puntos);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mparam1 = getArguments().getString(ARG_NICKNAME);
            mparam2 = getArguments().getString(ARG_NOMBRE);
            mparam3 = getArguments().getString(ARG_PUNTOS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_juego, container, false);
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);

        final TextView nickname = vista.findViewById(R.id.nicknameTxt);
        final TextView nombre = vista.findViewById(R.id.nombreTxt);
        final TextView puntos = vista.findViewById(R.id.puntosTxt);
        String nicknameStr=pref.getString("nickname", null);
        String nombreStr=pref.getString("nombre", null);

        nickname.setText(nicknameStr);
        nombre.setText(nombreStr);
        puntos.setText("0");
        // Inflate the layout for this fragment
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
