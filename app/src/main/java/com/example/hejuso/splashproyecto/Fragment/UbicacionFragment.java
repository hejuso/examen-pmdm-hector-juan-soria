package com.example.hejuso.splashproyecto.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hejuso.splashproyecto.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import static android.content.Context.MODE_PRIVATE;

public class UbicacionFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    // TODO: Rename and change types of parameters

    private OnFragmentInteractionListener mListener;
    private GoogleMap gMap;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;

    public UbicacionFragment() {
        // Required empty public constructor
    }

    public static UbicacionFragment newInstance() {
        UbicacionFragment fragment = new UbicacionFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ubicacion, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_google);

        mapFragment.getMapAsync(this);

        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        gMap = map;

        setUpMap();

    }

    public void setUpMap(){

        gMap.setLatLngBoundsForCameraTarget(new LatLngBounds(
                new LatLng(39.150697, -0.430389), new LatLng(39.150697, -0.430389)));
        SharedPreferences pref = getView().getContext().getSharedPreferences("MyPref", MODE_PRIVATE);

        String nicknameStr=pref.getString("nickname", null);

        gMap.addMarker(new MarkerOptions().position(new LatLng(39.150697, -0.430389)).title("Hola, soc "+nicknameStr));
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        gMap.animateCamera( CameraUpdateFactory.zoomTo( 15.0f ) );
        //gMap.setMyLocationEnabled(true);
        gMap.setTrafficEnabled(true);
        gMap.setIndoorEnabled(true);
        //gMap.setBuildingsEnabled(true);
        gMap.getUiSettings().setZoomControlsEnabled(true);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onLocationChanged(Location location) {

    }

}
