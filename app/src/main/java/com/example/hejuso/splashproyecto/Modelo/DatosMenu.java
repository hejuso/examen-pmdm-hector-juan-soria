package com.example.hejuso.splashproyecto.Modelo;

/**
 * Created by hejuso on 06/12/2017.
 */

public class DatosMenu {
    private String titol;
    private int imatge;

    public DatosMenu(String titol, int imatge){
        this.titol = titol;
        this.imatge = imatge;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public int getImatge() {
        return imatge;
    }

    public void setImatge(int imatge) {
        this.imatge = imatge;
    }
}