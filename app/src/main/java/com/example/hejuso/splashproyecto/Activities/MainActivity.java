package com.example.hejuso.splashproyecto.Activities;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.hejuso.splashproyecto.Fragment.EstaticFragment;
import com.example.hejuso.splashproyecto.Fragment.JuegoFragment;
import com.example.hejuso.splashproyecto.Fragment.PefilFragment;
import com.example.hejuso.splashproyecto.Fragment.SelfieFragment;
import com.example.hejuso.splashproyecto.Fragment.UbicacionFragment;
import com.example.hejuso.splashproyecto.Fragment.WelcomeFragment;
import com.example.hejuso.splashproyecto.R;


public class MainActivity extends AppCompatActivity implements EstaticFragment.OnFragmentInteractionListener, WelcomeFragment.OnFragmentInteractionListener,
        PefilFragment.OnFragmentInteractionListener, JuegoFragment.OnFragmentInteractionListener, SelfieFragment.OnFragmentInteractionListener, UbicacionFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EstaticFragment fragment = EstaticFragment.newInstance();
        WelcomeFragment fragment_2 = WelcomeFragment.newInstance();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //Le pasamos el ID del Fragment del activity_menu.xml
        fragmentTransaction.add(R.id.frameMenu,fragment);
        fragmentTransaction.add(R.id.frameMenu_2,fragment_2);
        fragmentTransaction.commit(); //Lanzamos el fragment en nuestra Activity Menu_Activity

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
