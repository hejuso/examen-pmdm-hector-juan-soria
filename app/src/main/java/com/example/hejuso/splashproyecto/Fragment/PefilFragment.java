package com.example.hejuso.splashproyecto.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.hejuso.splashproyecto.R;

import static android.content.Context.MODE_PRIVATE;

public class PefilFragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;

    public PefilFragment() {
        // Required empty public constructor
    }

    public static PefilFragment newInstance() {
        PefilFragment fragment = new PefilFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pefil, container, false);

        Button btn = v.findViewById(R.id.btn_submit);
        btn.setOnClickListener(this);
        // Inflate the layout for this fragment
        return v;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onClick(View v) {
        EditText nombre = getView().findViewById(R.id.nombapell);
        EditText nickname = getView().findViewById(R.id.nickname);

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString("nombre",nombre.getText().toString());
        editor.putString("nickname",nickname.getText().toString());
        editor.commit();

        AppCompatActivity act = (AppCompatActivity) v.getContext();
        Fragment fragmentWelcome = new WelcomeFragment();
        act.getSupportFragmentManager().beginTransaction().replace(R.id.frameMenu_2, fragmentWelcome).addToBackStack(null).commit();

    }


}
